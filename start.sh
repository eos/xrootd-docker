#!/bin/bash

image=""
while getopts 'i:' flag; do
  case "${flag}" in
    i) image="${OPTARG}" ;;
    *) echo "Unexpected option ${flag}" ;;
  esac
done

if [[ $image == "" ]]; then
  echo "Usage: -i <name of the docker image>"
  exit 1
fi

./get_data.sh

docker network create xrootd.cern.ch

docker run -dit --privileged -e "container=docker" -v /home/simonm/xrootd/packaging/rpms/:/xrootd-ci-repo:z -v $(pwd)/data:/downloads:z --name metaman -h metaman --net=xrootd.cern.ch --net-alias=multiip --net-alias=metaman $image /sbin/init
echo "[Status] metaman container: started!"
docker run -dit --privileged -e "container=docker" -v $(pwd)/data:/downloads:z --name man1 -h man1 --net=xrootd.cern.ch --net-alias=multiip --net-alias=man1 $image /sbin/init
echo "[Status] man1 container: started!"
docker run -dit --privileged -e "container=docker" -v $(pwd)/data:/downloads:z --name man2 -h man2 --net=xrootd.cern.ch --net-alias=multiip --net-alias=man2 $image /sbin/init
echo "[Status] man2 container: started!"
docker run -dit --privileged -e "container=docker" -v $(pwd)/data:/downloads:z --name srv1 -h srv1 --net=xrootd.cern.ch --net-alias=multiip --net-alias=srv1 $image /sbin/init
echo "[Status] srv1 container: started!"
docker run -dit --privileged -e "container=docker" -v $(pwd)/data:/downloads:z --name srv2 -h srv2 --net=xrootd.cern.ch --net-alias=multiip --net-alias=srv2 $image /sbin/init
echo "[Status] srv2 container: started!"
docker run -dit --privileged -e "container=docker" -v $(pwd)/data:/downloads:z --name srv3 -h srv3 --net=xrootd.cern.ch --net-alias=multiip --net-alias=srv3 $image /sbin/init
echo "[Status] srv3 container: started!"
docker run -dit --privileged -e "container=docker" -v $(pwd)/data:/downloads:z --name srv4 -h srv4 --net=xrootd.cern.ch --net-alias=multiip --net-alias=srv4 $image /sbin/init
echo "[Status] srv4 container: started!"

docker exec metaman /usr/local/bin/setup_container.sh
echo "[Status] metaman container: setup!"
docker exec man1    /usr/local/bin/setup_container.sh
echo "[Status] man1 container: setup!"
docker exec man2    /usr/local/bin/setup_container.sh
echo "[Status] man2 container: setup!"
docker exec srv1    /usr/local/bin/setup_container.sh
echo "[Status] srv1 container: setup!"
docker exec srv2    /usr/local/bin/setup_container.sh
echo "[Status] srv2 container: setup!"
docker exec srv3    /usr/local/bin/setup_container.sh
echo "[Status] srv3 container: setup!"
docker exec srv4    /usr/local/bin/setup_container.sh
echo "[Status] srv4 container: setup!"

docker exec metaman systemctl start xrootd@clustered
docker exec metaman systemctl start cmsd@clustered
echo "[Status] metaman container: xrootd up and running!"
docker exec man1    systemctl start xrootd@clustered
docker exec man1    systemctl start cmsd@clustered
echo "[Status] man1 container: xrootd up and running!"
docker exec man2    systemctl start xrootd@clustered
docker exec man2    systemctl start cmsd@clustered
echo "[Status] man2 container: xrootd up and running!"
docker exec srv1    systemctl start xrootd@clustered
docker exec srv1    systemctl start cmsd@clustered
echo "[Status] srv1 container: xrootd up and running!"
docker exec srv2    systemctl start xrootd@srv2
docker exec srv2    systemctl start xrootd@clustered
docker exec srv2    systemctl start cmsd@clustered
echo "[Status] srv2 container: xrootd up and running!"
docker exec srv3    systemctl start xrootd@clustered
docker exec srv3    systemctl start cmsd@clustered
echo "[Status] srv3 container: xrootd up and running!"
docker exec srv4    systemctl start xrootd@clustered
docker exec srv4    systemctl start cmsd@clustered
echo "[Status] srv4 container: xrootd up and running!"
