#
# Jenkins master
#
# Version 0.1

FROM cern/cc7-base
MAINTAINER Michal Simon <michal.simon@cern.ch>, CERN, 2015

USER root
# copy castor continuous integration YUM repo
COPY xrootd-ci.repo /etc/yum.repos.d/xrootd-ci.repo
RUN /usr/bin/yum -y --nogpgcheck install yum-plugin-priorities
# exclude xrootd from epel
RUN awk '/\[epel\]/{ print; print "exclude=xrootd*"; next }1' /etc/yum.repos.d/epel.repo > epel.repo ; mv epel.repo /etc/yum.repos.d/epel.repo
# install the RPM with tests and the CERN tnsnames.ora file, which will point to the location of c2castordevdb on the network
RUN /usr/bin/yum --disablerepo=cern -y --nogpgcheck install xrootd xrootd-client xrootd-tests xrootd-debuginfo vim wget gdb xterm 
# copy the configuration file for xrootd
COPY xrootd-test.cfg /etc/xrootd/xrootd-clustered.cfg
# copy the additional config file for srv2
COPY xrootd-srv2.cfg /etc/xrootd/xrootd-srv2.cfg
# copy the setup script
COPY setup_container.sh /usr/local/bin/setup_container.sh
