#!/bin/bash
docker rm -f metaman man1 man2 srv1 srv2 srv3 srv4
docker network rm xrootd.cern.ch
docker rmi $(docker images -f "dangling=true" -q)

exit 0
