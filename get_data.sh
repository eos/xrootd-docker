#!/bin/bash

if [ ! -d ./data ]; then
  echo "[Status] create ./data directory"
  mkdir ./data
fi

if [ ! -f ./data/1db882c8-8cd6-4df1-941f-ce669bad3458.dat ]; then
  echo "[Status] download 1db882c8-8cd6-4df1-941f-ce669bad3458.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/1db882c8-8cd6-4df1-941f-ce669bad3458.dat --directory-prefix ./data
fi

if [ ! -f ./data/3c9a9dd8-bc75-422c-b12c-f00604486cc1.dat ]; then
  echo "[Status] download 3c9a9dd8-bc75-422c-b12c-f00604486cc1.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/3c9a9dd8-bc75-422c-b12c-f00604486cc1.dat --directory-prefix ./data
fi

if [ ! -f ./data/7e480547-fe1a-4eaf-a210-0f3927751a43.dat ]; then
  echo "[Status] download 7e480547-fe1a-4eaf-a210-0f3927751a43.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/7e480547-fe1a-4eaf-a210-0f3927751a43.dat --directory-prefix ./data
fi

if [ ! -f ./data/7235b5d1-cede-4700-a8f9-596506b4cc38.dat ]; then
  echo "[Status] download 7235b5d1-cede-4700-a8f9-596506b4cc38.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/7235b5d1-cede-4700-a8f9-596506b4cc38.dat --directory-prefix ./data
fi
if [ ! -f ./data/89120cec-5244-444c-9313-703e4bee72de.dat ]; then
  echo "[Status] download 89120cec-5244-444c-9313-703e4bee72de.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/89120cec-5244-444c-9313-703e4bee72de.dat --directory-prefix ./data
fi

if [ ! -f ./data/a048e67f-4397-4bb8-85eb-8d7e40d90763.dat ]; then
  echo "[Status] download a048e67f-4397-4bb8-85eb-8d7e40d90763.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/a048e67f-4397-4bb8-85eb-8d7e40d90763.dat --directory-prefix ./data
fi

if [ ! -f ./data/b3d40b3f-1d15-4ad3-8cb5-a7516acb2bab.dat ]; then
  echo "[Status] download b3d40b3f-1d15-4ad3-8cb5-a7516acb2bab.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/b3d40b3f-1d15-4ad3-8cb5-a7516acb2bab.dat --directory-prefix ./data
fi

if [ ! -f ./data/b74d025e-06d6-43e8-91e1-a862feb03c84.dat ]; then
  echo "[Status] download b74d025e-06d6-43e8-91e1-a862feb03c84.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/b74d025e-06d6-43e8-91e1-a862feb03c84.dat --directory-prefix ./data
fi

if [ ! -f ./data/cb4aacf1-6f28-42f2-b68a-90a73460f424.dat ]; then
  echo "[Status] download cb4aacf1-6f28-42f2-b68a-90a73460f424.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/cb4aacf1-6f28-42f2-b68a-90a73460f424.dat --directory-prefix ./data
fi

if [ ! -f ./data/cef4d954-936f-4945-ae49-60ec715b986e.dat ]; then
  echo "[Status] download cef4d954-936f-4945-ae49-60ec715b986e.dat"
  wget -4 http://xrootd.cern.ch/tests/test-files/cef4d954-936f-4945-ae49-60ec715b986e.dat --directory-prefix ./data
fi

echo "[Status] Get data: done!"

